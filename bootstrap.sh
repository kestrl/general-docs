echo "Building\n"
mkdocs build

echo "Uploading files to s3\n"
aws s3 sync ./site s3://$S3_BUCKET --recursive