# Authentication

This section will explain how our client (mobile app) and our servers communicate with an authentication layer.

## Authorizing Clients

We have two types of authentication.

- Key pair of device to server.
- Password authentication by our user.

## JWT Token[^1]

JSON Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties.
Apart from that, Kestrl API is strict on the header payload when making calls:

```
x-kestrl-token: APP_SECURE_TOKEN
x-kestrl-app-version: APP_VERSION
x-kestrl-auth-token: APP_USER_TOKEN
```

| Field       | Description    | Encryption | Expires    |
|-------------|----------------|------------|------------|
| `APP_SECURE_TOKEN` | Required key for communication between client and server. | `SHA256` | 90 days |
| `APP_VERSION` | Client application version | - | - |
| `APP_USER_TOKEN` | Auth token generated after user login | `SHA256` | 7 days |

[^1]: JSON-based access token.
