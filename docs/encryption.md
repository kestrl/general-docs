# Encryption

This section describes how we encrypt, store and extract sensitive data in our server.

## Storing sensitive data

We store sensitive data in a server that can only be accessed via Public Key authentication[^1] with the type RSA and 4096 bit encryption algorithm.
We store data using novel hash functions computed with 64-bit words with `SHA256`.

## Extracting sensitive data

Extracting the data requires decrypting it with a specific hash key only known to our Data Protection Officer.
Steps to take for data extraction is detailed below:

- Request for extraction via email to the Data Protection Officer
- Upon approval by Kestrl Executives, a limited 20-days permission will be given on a need-to-know basis,.
- Every extraction will be automatically audited by the system.


[^1]: Asymmetric Cryptography on RSA/DSA algorithm. [Read more](https://www.ssh.com/ssh/public-key-authentication)