# Overview

## Platforms

Kestrl is a digital bank platform with an ecosystem that consist of the following platforms:

- Mobile Application[^1]
    - [iOS](https://apps.apple.com/us/app/kestrl-budget-money-tracker/id1512592353)
    - [Android](https://play.google.com/store/apps/details?id=uk.co.kestrl.android&hl=en_US&gl=US)
- <a href="https://www.kestrl.co.uk" target="_blank">Website</a>
- <a href="https://support.kestrl.co.uk/en/">Support Dashboard</a>
- Sabre: Intelligence & Analytics[^1]

## Infrastructure

The diagram below illustrates our infra architecture.

![image](https://user-images.githubusercontent.com/6908558/76098985-53a77900-5fc2-11ea-9776-58c5dea3a300.png)

External links to our resources:

- <a href="https://www.kestrl.co.uk" target="_blank">Official Website</a>
- <a href="https://hangout.kestrl.co.uk" target="_blank">Discussion Forum</a>
- <a href="https://documenter.getpostman.com/view/5470122/SVmvTeGx?version=latest" target="_blank">Kestrl API Documentation</a>
- <a href="https://dbdiagram.io/d/5e36451c9e76504e0ef0eb89" target="_blank">Kestrl Database Diagram</a>

[^1]: Under active development.