# Pixel Tracking

This is the guide on how third party application can push data to us on any events.
We require platforms to have their own unique `client_id` which will be supplied by Kestrl.

### The Endpoint

We have prepared two endpoints for you to use:

- Sandbox: `https://dev-x.kestrl.co.uk`
- Production: `https://api.kestrl.co.uk`

Please follow the format below for the pixel:

`API_URL + /track/<platform_name>.gif?client_id=xxx&scope=<scope>&id=<userid>`

| Parameters        | Description    | Type | Required |
|-------------------|----------------|------|----------|
| `client_id`       | Your client id. To get this, you'll need to be a verified Marketplace with Kestrl. | String | Yes |
| `scope`           | Events to send. Accepted values are: `click`, `signup` and `purchase`. Some Marketplace might have their own specific events. | String | Yes |
| `id`              | User ID. Kestrl will send this whenever we hit your endpoint. You'll need to store this so that it can be passed back through the pixel. | String | Yes |
| `amount`          | The purchase value of transactions made by the user. | Number | Only on `purchase` event |
| `investment_name` | The name of the product purchased by user. | String | Required only for specific Marketplace. |

### Implementation

- You can simply use the `<img>` tag or call the URL directly. It will return a white 1x1 gif file upon successful transaction.
- Otherwise it will return a 4xx error. In order to debug the error, copy the generated URL and paste it in a browser, or Postman and it will return a JSON with the error message.