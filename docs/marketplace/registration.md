# Register

In order for you to register and listed in our platform, please use this [Registration Form](https://forms.gle/bzKkEEaiCT2buSsB6).
You'll need to notify us at <a href="mailto:info@kestrl.co.uk">info@kestrl.co.uk</a> once you've submitted the form in order for us to register your platform in our database.

### Technical Requirements

- All marketplaces are required to implement and adhere to our [Pixel Tracking](pixel-tracking.md) method to track conversion and keep both records in sync.
- A record of traffic that we brought over to your platform, in any preferred frequency (e.g monthly, weekly, etc.)
- Conversion report.