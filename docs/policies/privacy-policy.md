# Privacy Policy

## Who are we
This privacy policy (the “Privacy Policy”) applies to all personal information Kestrl uses. Our principal address is [27 Old Gloucester Street, London WC1N 3AX](https://goo.gl/maps/Wsk9Z9qs56buCqYN8). We’re a companion bank account here to help you spend smarter, save more and manage your money better. This Privacy Policy explains how we collect and use your personal information to provide you with our products and services.
Keeping your personal information secure and safeguarding your privacy are really important to us. So is making sure you understand what personal information we use, and how we use it, where we get it from, who we share it with and what rights you have to control how we use it.

From time to time we may need to update this Privacy Policy. When we do, we’ll let you know and publish the updated version in the Kestrl app and website. It’s a good idea to read the Privacy Policy every time we make changes to it so you know exactly how we use your data and what your rights are.

## How we use your personal information

We use your personal information in three ways:

- To provide our products and services to you
- To meet our legal obligations
- To run our business

We have described these in greater detail in Schedule A - What we use your information for

## The information we use and where it comes from

We collect and use various categories of personal information but only to the extent we need to achieve one or more of the purposes listed above. This personal information includes:

- Personal information you supply through the Kestrl app or website (for example, when you signed up to Kestrl or when you use your account, or get in touch with us). This includes your name, contact details, ID and visual images (such as selfie or copy of passport photo).
- Transactional and other information we learn from how you use the Kestrl app, your card, your account, and any other third party bank accounts you’ve allowed us to access through account aggregation / Open Banking.
- Information we receive from third parties, such as those providing services to us or you e.g. ID verification services, credit reference agencies, fraud prevention or government agencies, and other banks.
- Device information including your location, mobile phone network, IP address and telephone number and how you use your mobile to access Kestrl
- Information about your family, lifestyle and social circumstances (such as dependents, marital status, next of kin and their contact details);
- Information about your financial circumstances, including personal wealth, assets and liabilities, proof of income and expenditure, credit and borrowing history and needs and goals.
- Online profile and social media information and activity, based on yourinteraction with us and our websites and app, including for example, yourbanking profile and login information, Internet Protocol (IP) address, smart device information, location coordinates, online and mobile bankingsecurity authentication, mobile phone network information, searches, sitevisits and spending patterns.
- Information from publicly available sources including social media profiles, the electoral register, the media and online search engines.
- We may also use certain special categories of information for specific and limited purposes, such as detecting and preventing financial crime or to make our services accessible to customers. We will only use special categories of information where we’ve obtained your explicit consent or are otherwise lawfully permitted (and then only for the particular purposes and activities set out in Schedule A for which the information is provided). This may include:

    - information about racial or ethnic origin,
    - religious or philosophical beliefs;
    - trade union membership;
    - physical or psychological health details or medical conditions; andbiometric information, relating to the physical, physiological or behavioural characteristics of a person, including, for example, using voice recognitionor similar technologies to help us prevent fraud and money laundering.

Where permitted by law, we may use information about criminal convictions or offences and alleged offences for specific and limited activities and purposes, such as to perform checks to prevent and detect crime and to comply with laws relating to money laundering, fraud, terrorist financing, bribery and corruption, and international sanctions. It may involve investigating and gatheringintelligence on suspected financial crimes, fraud and threats and sharing data between banks and with law enforcement and regulatory bodies.

Your transaction history and account information may also contain special categories of personal data. For example, if you have a payment for a membership to a particular political party, this could reveal your political beliefs. We will not profile you on the basis of this data or otherwise use this data for any other purposes other than providing our services.

## Your rights

Your rights in relation to the personal information we hold on you are set out in the table below. If you wish to exercise any of these rights, or if you have any queries about how we use your personal information which is not answered here, you can contact us [dbo@kestrl.co.uk](mailto:dbo@kestrl.co.uk)

Please note that in some cases, if you don’t agree with how we use your information, it may not be possible for us to continue to operate your Kestrl account and/or provide certain products and services to you.
